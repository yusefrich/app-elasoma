export const themes = {
  light: {
    buttons: {
      tag: {
        color: '#FFFFFF',
      },
    },
    card: {
      borderColor: '#F1F1F1',
    },
    chip: {
      backgroundColor: '#C4C4C4',
      textColor: '#fff',
      borderColor: '#C4C4C4',
      activeBackgroundColor: '#000',
      activeTextColor: '#fff',
      activeBorderColor: '#000000',
    },
    colors: {
      white: '#ffffff',
      dark: '#21102E',
      secondary: '#fff',
      tertiary: '#f3f3f3',
    },
    containerBackground: '#ffffff',
    drawer: {
      backgroundColor: '#ffffff',
      borderColor: '#F1F1F1',
      textColor: '#21102E',
      textProfileColor: '#21102E',
      exitTextColor: '#21102E',
    },
    header: {
      borderColor: '#d5d5d5',
    },
    inactiveButton: '#a2a2a2',
    input: {
      borderColor: '#000000',
      switch: {
        thumbColor: '#f1e8f3',
        darkMode: '#c7c9d9',
        lightMode: '#e37fe3',
        backgroundColor: '#e37fe3',
      },
    },
    primaryButton: '#21102E',
    primaryButtonColor: '#FFFFFF',
    sectionTitle: {
      backgroundColor: '#D6BBDB',
      spacingBackgroundColor: '#9096B4',
      color: '#21102E',
    },
    statusBar: {
      backgroundColor: '#ffffff',
      barStyle: 'dark-content',
    },
    textColor: '#21102E',
    textInputColor: '#21102E',
    textContrastColor: '#000000',
    themeName: 'light',
    activityIndicator: '#000',
  },
  dark: {
    buttons: {
      tag: {
        color: '#9096B4',
      },
    },
    card: {
      borderColor: '#151A2E',
    },
    chip: {
      backgroundColor: '#21102E',
      textColor: '#fff',
      borderColor: '#ffffff',
      activeBackgroundColor: '#fff',
      activeTextColor: '#21102E',
      activeBorderColor: '#ffffff',
    },
    containerBackground: '#21102E',
    colors: {
      white: '#ffffff',
      dark: '#21102E',
      secondary: '#21102E',
      tertiary: '#21102E',
    },
    drawer: {
      backgroundColor: '#21102E',
      borderColor: '#21102E',
      textColor: '#D6BBDB',
      textProfileColor: '#ffffff',
      exitTextColor: '#ffffff',
    },
    header: {
      borderColor: '#21102E',
    },
    inactiveButton: '#a2a2a2',
    input: {
      borderColor: '#FFFFFF',
      switch: {
        thumbColor: '#ffffff',
        darkMode: '#c7c9d9',
        lightMode: '#e37fe3',
        backgroundColor: '#e37fe3',
      },
    },
    primaryButton: '#D6BBDB',
    primaryButtonColor: '#21102E',
    sectionTitle: {
      backgroundColor: '#FFFFFF',
      spacingBackgroundColor: '#2f3347',
      color: '#21102E',
    },
    statusBar: {
      backgroundColor: '#21102E',
      barStyle: 'light-content',
    },
    textColor: '#ffffff',
    textInputColor: '#D6BBDB',
    textContrastColor: '#ffffff',
    themeName: 'dark',
    activityIndicator: '#9096B4',
  },
};
